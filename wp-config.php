<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'TeliaGallery' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'c?a7_18@9 dvm>??Q8}t2e?:t=@)%Jv}97&k;NxC;$_bZ9GGBVI<OYDe5{Uxn]gC' );
define( 'SECURE_AUTH_KEY',  'WeeE)Qny=,R3FoPhof@(F@F?V!~8Cl^G?aDRK3B+QEr2a5,SN8.P]=R]]8V<u><A' );
define( 'LOGGED_IN_KEY',    '64{m4ok]m1j?sIc<2m{,+.w~TlY_!tRdbD*c8/,GFE*j*US,-Imjg0CYfC_nL>0:' );
define( 'NONCE_KEY',        '>RZEpke0i!Z?phuUiHYS:2y7jb)Te!MTj@soc;&#:Yn&i7,s}}f> .Uigc]E)~RT' );
define( 'AUTH_SALT',        'ca<he1&mA=*`kM,gPsaDKHW5,$b`(wh^/$jWE@`+Z(F6@m$LK.8C^/y{OrwqrQ5I' );
define( 'SECURE_AUTH_SALT', 'xw6Z%O.K7E],Y~APie(ex0Haq_LN!g0U bC4Y|K =1k9>wB=J]Q,c}Ttpp|iH:* ' );
define( 'LOGGED_IN_SALT',   'FkB@o)Tmi]$]3}X+C>)oe=vr]!7>XdAM;]!mdS/Lu#a4JY>lLj{1~1iv;SA=~&Yg' );
define( 'NONCE_SALT',       'c8:yf4 1U6>y>s<VB0;(AsG10e*1L/?]o8URw(-{FgKN!nhT~|&lVZ>8@<OB8t*V' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
